#![warn(clippy::pedantic)]
#![allow(clippy::unreadable_literal)]

pub mod analysis;
pub mod errors;
pub mod games;
pub mod types;
pub mod util;
