use crate::analysis::eval::Eval;
use crate::analysis::hand_rank::HandRankClass;
use crate::analysis::indexed::Indexed;
use std::collections::HashSet;

/// `Evals` is a tuple struct used to hold unique hand evaluations.
#[derive(Clone, Debug, Default, PartialEq)]
pub struct Evals(Vec<Eval>, HashSet<HandRankClass>);

impl Evals {
    pub fn push(&mut self, evaluated_hand: Eval) {
        if self.1.insert(evaluated_hand.rank.class) {
            self.0.push(evaluated_hand);
        }
    }

    #[must_use]
    pub fn sort(&self) -> Evals {
        let mut cards = self.clone();
        cards.sort_in_place();
        cards
    }

    pub fn sort_in_place(&mut self) {
        self.0.sort_unstable();
        self.0.reverse();
    }

    #[must_use]
    pub fn vec(&self) -> &Vec<Eval> {
        &self.0
    }

    #[must_use]
    pub fn indexed(&self) -> Indexed {
        Indexed::from(self)
    }
}
