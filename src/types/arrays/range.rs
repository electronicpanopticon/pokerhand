use crate::errors::HandError;
use crate::types::arrays::two_cards::TwoCards;
use crate::types::PokerCard;
use ckc_rs::CardNumber;
use std::collections::HashSet;

/// TODO: Scenario: AK JTs 22 HSK S06E02
/// TODO: Scenario: A2 T9s HSK S06E03
pub struct Range {
    pub hands: HashSet<TwoCards>,
    cards: HashSet<PokerCard>,
}

impl Range {
    /// # Errors
    ///
    /// Throws a `HandError::DuplicateCard` error if a `PokerCard` passed in
    /// already exists in the `Range`.
    pub fn insert(&mut self, cards: TwoCards) -> Result<bool, HandError> {
        if self.contains_card(cards) {
            Err(HandError::DuplicateCard)
        } else {
            Ok(self.hands.insert(cards))
        }
    }

    #[must_use]
    pub fn contains_card(&self, cards: TwoCards) -> bool {
        self.cards.contains(&cards.first()) || self.cards.contains(&cards.second())
    }

    #[must_use]
    pub fn is_empty(&self) -> bool {
        self.hands.len() == 0
    }

    #[must_use]
    pub fn len(&self) -> usize {
        self.hands.len()
    }
}

impl Default for Range {
    fn default() -> Self {
        Range {
            hands: HashSet::new(),
            cards: vec![CardNumber::BLANK].into_iter().collect(),
        }
    }
}

// impl From<HashSet<TwoCards>> for Range {
//     fn from(value: HashSet<TwoCards>) -> Self {
//         Range(value)
//     }
// }

#[cfg(test)]
#[allow(non_snake_case)]
mod arrays_range_tests {
    use super::*;

    #[test]
    fn insert() {
        let mut range = Range::default();
        let two = TwoCards::try_from("AS AC").unwrap();

        let actual = range.insert(two);

        assert!(actual.is_ok());
        assert!(actual.unwrap());
    }

    #[test]
    fn insert__blank_card() {
        let mut range = Range::default();

        let err = range.insert(TwoCards::default());

        assert!(err.is_err());
    }

    #[test]
    fn is_empty() {
        assert!(Range::default().is_empty());
    }

    #[test]
    fn len() {
        assert_eq!(0, Range::default().len());
    }

    #[test]
    fn default() {
        let range = Range::default();

        assert!(range.is_empty());
        assert_eq!(1, range.cards.len());
    }
}
