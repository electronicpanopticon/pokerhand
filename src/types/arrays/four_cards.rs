use crate::errors::HandError;
use crate::types::arrays::two_cards::TwoCards;
use crate::types::arrays::Vectorable;
use crate::types::playing_card::PlayingCard;
use crate::types::poker_cards::PokerCards;
use crate::types::PokerCard;
use cardpack::Pile;
use std::fmt;

#[derive(Clone, Copy, Debug, Default, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct FourCards(pub [PokerCard; 4]);

impl FourCards {
    /// permutations to evaluate all `Omaha` style combinations.
    pub const PERMUTATIONS: [[u8; 2]; 6] = [[0, 1], [0, 2], [0, 3], [1, 2], [1, 3], [2, 3]];

    #[must_use]
    pub fn permutation(&self, permutation: [u8; 2]) -> TwoCards {
        TwoCards::from([
            self.0[permutation[0] as usize],
            self.0[permutation[1] as usize],
        ])
    }

    #[must_use]
    pub fn first(&self) -> PokerCard {
        self.0[0]
    }

    #[must_use]
    pub fn second(&self) -> PokerCard {
        self.0[1]
    }

    #[must_use]
    pub fn third(&self) -> PokerCard {
        self.0[2]
    }

    #[must_use]
    pub fn forth(&self) -> PokerCard {
        self.0[3]
    }

    #[must_use]
    pub fn to_arr(&self) -> [PokerCard; 4] {
        self.0
    }
}

impl fmt::Display for FourCards {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.to_poker_cards())
    }
}

impl From<[PlayingCard; 4]> for FourCards {
    fn from(array: [PlayingCard; 4]) -> Self {
        FourCards::from([
            array[0].as_poker_card(),
            array[1].as_poker_card(),
            array[2].as_poker_card(),
            array[3].as_poker_card(),
        ])
    }
}

impl From<[PokerCard; 4]> for FourCards {
    fn from(value: [PokerCard; 4]) -> Self {
        FourCards(value)
    }
}

impl TryFrom<&PokerCards> for FourCards {
    type Error = HandError;

    fn try_from(value: &PokerCards) -> Result<Self, Self::Error> {
        FourCards::try_from(value.to_vec())
    }
}

impl TryFrom<&'static str> for FourCards {
    type Error = HandError;

    fn try_from(value: &'static str) -> Result<Self, Self::Error> {
        match PokerCards::try_from(value) {
            Ok(cards) => FourCards::try_from(&cards),
            Err(e) => Err(e),
        }
    }
}

impl TryFrom<Vec<PokerCard>> for FourCards {
    type Error = HandError;

    fn try_from(value: Vec<PokerCard>) -> Result<Self, Self::Error> {
        match value.len() {
            0..=3 => Err(HandError::NotEnoughCards),
            4 => {
                let cards: [PokerCard; 4] = value.try_into().unwrap_or_else(|v: Vec<PokerCard>| {
                    panic!("Expected a Vec of length {} but it was {}", 4, v.len())
                });
                Ok(FourCards::from(cards))
            }
            _ => Err(HandError::TooManyCards),
        }
    }
}

impl Vectorable for FourCards {
    #[must_use]
    fn to_vec(&self) -> Vec<PokerCard> {
        self.0.to_vec()
    }

    #[must_use]
    fn to_pile(&self) -> Pile {
        PokerCards::from(self.to_vec()).to_pile()
    }

    #[must_use]
    fn to_poker_cards(&self) -> PokerCards {
        PokerCards::from(self.0.to_vec())
    }
}

#[cfg(test)]
#[allow(non_snake_case)]
mod types_arrays_four_cards_tests {
    use super::*;

    #[test]
    fn permutation() {
        let four = FourCards::try_from("5♠ Q♥ 6♠ AC").unwrap();

        assert_eq!(
            "5♠ Q♥",
            four.permutation(FourCards::PERMUTATIONS[0]).to_string()
        );
        assert_eq!(
            "5♠ 6♠",
            four.permutation(FourCards::PERMUTATIONS[1]).to_string()
        );
        assert_eq!(
            "5♠ A♣",
            four.permutation(FourCards::PERMUTATIONS[2]).to_string()
        );
        assert_eq!(
            "Q♥ 6♠",
            four.permutation(FourCards::PERMUTATIONS[3]).to_string()
        );
        assert_eq!(
            "Q♥ A♣",
            four.permutation(FourCards::PERMUTATIONS[4]).to_string()
        );
        assert_eq!(
            "6♠ A♣",
            four.permutation(FourCards::PERMUTATIONS[5]).to_string()
        );
    }
}
