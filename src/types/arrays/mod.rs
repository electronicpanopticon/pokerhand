use crate::analysis::eval::Eval;
use crate::analysis::hand_rank::HandRank;
use crate::types::arrays::five_cards::FiveCards;
use crate::types::poker_cards::PokerCards;
use crate::types::PokerCard;
use cardpack::Pile;

pub mod evaluable;
pub mod five_cards;
pub mod four_cards;
pub mod range;
pub mod range_vector;
pub mod seven_cards;
pub mod six_cards;
pub mod two_cards;

pub trait Evaluable {
    fn evaluate(&self) -> (FiveCards, HandRank);

    fn eval(&self) -> Eval {
        let (hand, rank) = self.evaluate();
        Eval::new(hand, rank)
    }
}

pub trait Vectorable {
    fn to_vec(&self) -> Vec<PokerCard>;

    fn to_pile(&self) -> Pile;

    fn to_poker_cards(&self) -> PokerCards;
}
