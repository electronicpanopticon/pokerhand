use crate::errors::HandError;
use crate::types::arrays::Vectorable;
use crate::types::playing_card::PlayingCard;
use crate::types::playing_cards::PlayingCards;
use crate::types::poker_cards::PokerCards;
use crate::types::slots::hole_cards::HoleCards;
use crate::types::PokerCard;
use cardpack::Pile;
use ckc_rs::CardNumber;
use serde::{Deserialize, Serialize};
use std::fmt;

#[derive(
    Serialize, Deserialize, Clone, Copy, Debug, Default, Eq, Hash, Ord, PartialEq, PartialOrd,
)]
pub struct TwoCards([PokerCard; 2]);

impl TwoCards {
    /// Factory method that ensures that the two cards are different, and neither
    /// is blank. Creates an array sorted their values.
    ///
    /// # Errors
    ///
    /// Throws a `HandError::DuplicateCard` error if the two cards are identical.
    #[allow(clippy::comparison_chain)]
    pub fn new(first: PokerCard, second: PokerCard) -> Result<TwoCards, HandError> {
        if first == second {
            Err(HandError::DuplicateCard)
        } else if first == CardNumber::BLANK || second == CardNumber::BLANK {
            Err(HandError::BlankCard)
        } else if first > second {
            Ok(TwoCards([
                PlayingCard::from(first).as_poker_card(),
                PlayingCard::from(second).as_poker_card(),
            ]))
        } else {
            Ok(TwoCards([
                PlayingCard::from(second).as_poker_card(),
                PlayingCard::from(first).as_poker_card(),
            ]))
        }
    }

    #[must_use]
    pub fn first(&self) -> PokerCard {
        self.0[0]
    }

    #[must_use]
    pub fn is_dealt(&self) -> bool {
        (self.0[0] != CardNumber::BLANK) && (self.0[1] != CardNumber::BLANK)
    }

    #[must_use]
    pub fn second(&self) -> PokerCard {
        self.0[1]
    }

    #[must_use]
    pub fn to_arr(&self) -> [PokerCard; 2] {
        self.0
    }
}

impl Vectorable for TwoCards {
    #[must_use]
    fn to_vec(&self) -> Vec<PokerCard> {
        self.0.to_vec()
    }

    #[must_use]
    fn to_pile(&self) -> Pile {
        PokerCards::from(self.to_vec()).to_pile()
    }

    #[must_use]
    fn to_poker_cards(&self) -> PokerCards {
        PokerCards::from(self.0.to_vec())
    }
}

impl fmt::Display for TwoCards {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{} {}",
            PlayingCard::from(self.0[0]),
            PlayingCard::from(self.0[1])
        )
    }
}

impl From<[PokerCard; 2]> for TwoCards {
    fn from(array: [PokerCard; 2]) -> Self {
        TwoCards(array)
    }
}

impl TryFrom<HoleCards> for TwoCards {
    type Error = HandError;

    fn try_from(value: HoleCards) -> Result<Self, Self::Error> {
        TwoCards::new(
            value.get_first_card().as_poker_card(),
            value.get_second_card().as_poker_card(),
        )
    }
}

impl TryFrom<&'static str> for TwoCards {
    type Error = HandError;

    /// # Errors
    ///
    /// Will return `CardError::InvalidCard` for an invalid index.
    #[allow(clippy::missing_panics_doc)]
    fn try_from(value: &'static str) -> Result<Self, Self::Error> {
        match PlayingCards::try_from(value) {
            Ok(cards) => {
                if cards.len() == 2 {
                    TwoCards::new(
                        cards.get_index(0).unwrap().as_poker_card(),
                        cards.get_index(1).unwrap().as_poker_card(),
                    )
                } else {
                    Err(HandError::InvalidCardCount)
                }
            }
            Err(e) => Err(e),
        }
    }
}

#[cfg(test)]
#[allow(non_snake_case)]
mod arrays_two_cards_tests {
    use super::*;

    #[test]
    fn new() {
        let cards = TwoCards::new(CardNumber::ACE_CLUBS, CardNumber::ACE_SPADES);

        assert!(cards.is_ok());
        assert_eq!("A♠ A♣", cards.unwrap().to_string());
    }

    #[test]
    fn new__blank() {
        let err = TwoCards::new(CardNumber::ACE_CLUBS, CardNumber::BLANK);

        assert!(err.is_err());
        assert_eq!(err.unwrap_err(), HandError::BlankCard);
    }

    #[test]
    fn new__duplicate() {
        let err = TwoCards::new(CardNumber::ACE_CLUBS, CardNumber::ACE_CLUBS);

        assert!(err.is_err());
        assert_eq!(err.unwrap_err(), HandError::DuplicateCard);
    }

    #[test]
    fn array() {
        assert_eq!([0, 0], TwoCards::default().to_arr());
    }

    #[test]
    fn display() {
        let two = TwoCards::default();

        assert_eq!("__ __", two.to_string());
    }

    #[test]
    fn try_from__index() {
        let two = TwoCards::try_from("AS AC").unwrap();

        assert_eq!("A♠ A♣", two.to_string());
    }

    #[test]
    fn try_from__index_duplicate() {
        let err = TwoCards::try_from("AC AC");

        assert!(err.is_err());
        assert_eq!(err.unwrap_err(), HandError::InvalidCardCount);
    }

    #[test]
    fn try_from__index__sort() {
        assert_eq!("A♠ A♣", TwoCards::try_from("AC AS").unwrap().to_string());
        assert_eq!("K♣ 9♠", TwoCards::try_from("9S KC").unwrap().to_string());
    }
}
