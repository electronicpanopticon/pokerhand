#[derive(Debug, PartialEq)]
pub enum HandError {
    BlankCard,
    DuplicateCard,
    Incomplete,
    InvalidCard,
    InvalidCardCount,
    InvalidIndex,
    NotEnoughCards,
    TooManyCards,
}
